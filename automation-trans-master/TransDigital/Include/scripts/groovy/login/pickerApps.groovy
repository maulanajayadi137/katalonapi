import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.eclipse.persistence.internal.oxm.record.json.JSONParser.object_return
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.Cookie
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import com.kms.katalon.core.logging.KeywordLogger
import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import static org.assertj.core.api.Assertions.*




class pickerApps{


	@Given('User_Login_PickerApps')
	def HIT_API_User_Login_PickerApps() {
		Object GetToken = WS.sendRequest(findTestObject('Object Repository/1_User/1_Login_UserPickApps'))
		WS.verifyResponseStatusCode(GetToken, 200)
		println JsonOutput.prettyPrint(GetToken.getResponseText())

		//		GetToken =WS.sendRequest(findTestObject('1_Login_UserPickApps'))
		//		WS.verifyResponseStatusCode(GetToken, 200)
		//		println(GetToken.getResponseText())
		//
		//	tokenGen().toString()
		//
		//


		//		GlobalVariable.token1 = token
		//		String token = WebUI.callTestCase(findTestCase('Salesforce API Testing/AccessTokenGeneration'), [:], FailureHandling.CONTINUE_ON_FAILURE)
		//
		//		println(token)
		//
		//		'Scope to a project'
		//
		//		def request = (RequestObject)findTestObject('Object Repository/LeadCreation')
		//
		//		'Create new ArrayList'
		//
		//		ArrayList<TestObjectProperty> HTTPHeader = new ArrayList<TestObjectProperty>()
		//
		//		'Send token in HTTP header'
		//
		//		HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "Bearer" + token))
		//
		//		HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))
		//
		//		'Set that token'
		//
		//		request.setHttpHeaderProperties(HTTPHeader)
		//
		//		'Get response text'
		//
		//		response = WS.sendRequest(findTestObject('Object Repository/LeadCreation'))
		//
		//		WS.verifyResponseStatusCode(response,200)
	}

	@Given('User_Login_PickerApps_MultiUser')
	def HIT_API_User_Login_PickerApps_MultiUser() {

		Object GetToken = WS.sendRequest(findTestObject('Object Repository/User/2_Login_UserPickApps-2'))

		WS.verifyResponseStatusCode(GetToken, 200)

		println JsonOutput.prettyPrint(GetToken.getResponseText())
	}

	@Given('User_get_orders_headers')
	def HIT_API_get_orders_headers() {
		Object Get_Headers = WS.sendRequest(findTestObject('Object Repository/Order/1_get_orders_headers'))
		WS.verifyResponseStatusCode(Get_Headers, 200)
		//     	WS.verifyElementPropertyValue(Get_Headers,'user_id',"self")
		println JsonOutput.prettyPrint(Get_Headers.getResponseText())

	}

	@Given('User_get_orders_details')
	def HIT_API_User_get_orders_details() {
		Object get_OrdersDetails = WS.sendRequest(findTestObject('Object Repository/Order/2_get_orders_details'))
		WS.verifyResponseStatusCode(get_OrdersDetails, 200)
		println JsonOutput.prettyPrint(get_OrdersDetails.getResponseText())
	}

	@Given('User_check_order_not_mine')
	def HIT_API_User_check_order_not_mine() {
		Object check_order = WS.sendRequest(findTestObject('Object Repository/Order/check_order_not_mine'))
		WS.verifyResponseStatusCode(check_order, 200)
		println JsonOutput.prettyPrint(check_order.getResponseText())
	}
	@Given('User_get_all_my_task')
	def HIT_API_User_get_all_my_task() {
		Object get_all_mytask = WS.sendRequest(findTestObject('Object Repository/Task/get_all_my_task'))
		WS.verifyResponseStatusCode(get_all_mytask , 200)
		println JsonOutput.prettyPrint(get_all_mytask.getResponseText())
	}

	@Given('User_create_my_task')
	def HIT_API_User_create_my_task() {
		Object create_task = WS.sendRequest(findTestObject('Object Repository/Task/3_create_task'))
		WS.verifyResponseStatusCode(create_task, 200)
		println JsonOutput.prettyPrint(create_task.getResponseText())
	}


	@Given('User_add_order_to_mytask')
	def HIT_API_User_add_order_to_mytask() {
		Object order_to_mytask = WS.sendRequest(findTestObject('Object Repository/Task/add_order_to_mytask'))
		WS.verifyResponseStatusCode(order_to_mytask, 202)
		println JsonOutput.prettyPrint(order_to_mytask.getResponseText())
	}
}

