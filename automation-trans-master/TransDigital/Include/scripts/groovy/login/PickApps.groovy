import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.math.RoundingMode
import java.text.DecimalFormat

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonBuilder

import static org.assertj.core.api.Assertions.*


import com.kms.katalon.core.testobject.ResponseObject as ResponseObject

import com.kms.katalon.core.webservice.verification.WSResponseManager as WSResponseManager

import groovy.json.JsonSlurper as JsonSlurper

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import org.openqa.selenium.Cookie




tokenGen().toString()



def token

String tokenGen() {

	def response = WS.sendRequest(findTestObject('GenerateAccessToken'))

	def jsonSlurper = new JsonSlurper()

	def slurper = new JsonSlurper()

	def result = slurper.parseText(response.getResponseBodyContent())

	def token = result.access_token

	WS.verifyResponseStatusCode(response, 200)

	println(token)

	GlobalVariable.token = token
}

def prices = WebUI.callTestCase(findTestCase('API/Egold/Dashboard/GOLD_API0001_GetGoldPrice'), [:], FailureHandling.CONTINUE_ON_FAILURE)
//rounding down
DecimalFormat goldFormat = new DecimalFormat("#0.0000")
goldFormat.setRoundingMode(RoundingMode.DOWN)
Double weight = Double.parseDouble(goldFormat.format(1000/prices[1]))

RequestObject req = findTestObject('Object Repository/API/Egold/Request')
req.setRestUrl(GlobalVariable.url_link +'/emas/api/sell/sell_validate')
req.setRestRequestMethod('POST')
raw = [
	"amount": 1000,
	"weight": weight,
	"sell_all": false
]
username = "caroline.advensia+sp01@tokopedia.com"
password = "Test123456"
def cookie = CustomKeywords.'tkpd.fintech.TokopediaLogin.getCookie'("API", username, password)
def input = new JsonBuilder(raw).toPrettyString()
HttpTextBodyContent body = new HttpTextBodyContent(input)
req.setBodyContent(body)
def header = CustomKeywords.'tkpd.fintech.api.SetHeaderFromMap.set'(["Host":"www.tokopedia.com", "Cookie":cookie])
req.setHttpHeaderProperties(header)
res = WS.sendRequest(req)
println res.getResponseText()
WebUI.callTestCase(findTestCase('API/Egold/Dashboard/Verify_Default'), [('response') : res], FailureHandling.CONTINUE_ON_FAILURE)