<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>toggle</name>
   <tag></tag>
   <elementGuidId>ff8671f6-eea9-4845-ae53-b336e4318bd5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='d-lg-none navbar-toggler']//span[@class='navbar-toggler-icon']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'navbar-toggler-icon']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-toggler-icon</value>
   </webElementProperties>
</WebElementEntity>
