<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>1_Login_UserPickApps</name>
   <tag></tag>
   <elementGuidId>d1aaff02-5ce0-4a76-8cd8-87617ef5c15b</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;username\&quot;: \&quot;picker_apps@ctcd.com\&quot;,\n\t\&quot;password\&quot;: \&quot;Password123@\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;text/plain&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>text/plain</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://pickpack-core-api-stg.ctdigital.id</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'http://35.244.131.219/pickpack/api/core-api'</defaultValue>
      <description></description>
      <id>cc639ae2-4b0d-4b1f-b546-d94db3e149a7</id>
      <masked>false</masked>
      <name>Core_Base_Url</name>
   </variables>
   <variables>
      <defaultValue>'/pickpack/login/v1.0'</defaultValue>
      <description></description>
      <id>d9215e6f-f522-4073-8c46-89abfb2fe1d2</id>
      <masked>false</masked>
      <name>UrlLogin</name>
   </variables>
   <variables>
      <defaultValue>'${Core_Base_Url}:${UrlLogin}'</defaultValue>
      <description></description>
      <id>1aebdc01-06d1-45df-b8cf-cfd7de2b2b29</id>
      <masked>false</masked>
      <name>EndpointCore</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()


</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
