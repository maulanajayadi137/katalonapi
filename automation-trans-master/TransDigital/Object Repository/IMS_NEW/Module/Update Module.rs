<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Module</name>
   <tag></tag>
   <elementGuidId>37e0ff92-89a6-458e-b7dd-ba7f2320f418</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;module_id\&quot;: 3,\n    \&quot;module_name\&quot;: \&quot;Location Management\&quot;,\n    \&quot;module_parent\&quot;: 0,\n    \&quot;module_privilege\&quot;: [\n        {\n            \&quot;privilege_id\&quot;: 1,\n            \&quot;privilege_name\&quot;: \&quot;VIEW\&quot;,\n            \&quot;selected\&quot;: true\n        },\n        {\n            \&quot;privilege_id\&quot;: 2,\n            \&quot;privilege_name\&quot;: \&quot;CREATE\&quot;,\n            \&quot;selected\&quot;: true\n        },\n        {\n            \&quot;privilege_id\&quot;: 3,\n            \&quot;privilege_name\&quot;: \&quot;UPDATE\&quot;,\n            \&quot;selected\&quot;: true\n        },\n        {\n            \&quot;privilege_id\&quot;: 4,\n            \&quot;privilege_name\&quot;: \&quot;DELETE\&quot;,\n            \&quot;selected\&quot;: true\n        },\n        {\n            \&quot;privilege_id\&quot;: 5,\n            \&quot;privilege_name\&quot;: \&quot;DOWNLOAD\&quot;,\n            \&quot;selected\&quot;: false\n        },\n        {\n            \&quot;privilege_id\&quot;: 6,\n            \&quot;privilege_name\&quot;: \&quot;UPLOAD\&quot;,\n            \&quot;selected\&quot;: true\n        }\n    ]\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${access_token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>${destination}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${ims_url}/v1/module</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.ims_url</defaultValue>
      <description></description>
      <id>22a1e2df-cf95-4314-9e33-78bd8c3c58d2</id>
      <masked>false</masked>
      <name>ims_url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.access_token</defaultValue>
      <description></description>
      <id>5e65f116-edd8-4274-a1a3-5d0b64fc9ea4</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.destination</defaultValue>
      <description></description>
      <id>725546da-44ad-4df4-94cf-674fa4349487</id>
      <masked>false</masked>
      <name>destination</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
