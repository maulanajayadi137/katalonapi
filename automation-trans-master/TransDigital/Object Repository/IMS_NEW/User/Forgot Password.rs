<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Forgot Password</name>
   <tag></tag>
   <elementGuidId>af0f263c-495f-42d4-80ce-6277ee6fc7bd</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;email\&quot;: \&quot;reca.short@gmail.com\&quot;\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>${destination}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${ims_url}/v1/user/forgot</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.ims_url</defaultValue>
      <description></description>
      <id>06ec9a21-5f03-4ee6-93bc-3ae98d376336</id>
      <masked>false</masked>
      <name>ims_url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.destination</defaultValue>
      <description></description>
      <id>c5eb8e2a-2aff-4d8c-8844-456a7d7a1251</id>
      <masked>false</masked>
      <name>destination</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
