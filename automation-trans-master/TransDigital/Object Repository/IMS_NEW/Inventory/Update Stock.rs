<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Stock</name>
   <tag></tag>
   <elementGuidId>12fde528-aaab-4e5b-85cb-7a7c1c5feabc</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;product_sku\&quot;: \&quot;PIM0000011976\&quot;,\n    \&quot;store_code\&quot;: \&quot;EAG\&quot;,\n    \&quot;quantity\&quot;: 10,\n    \&quot;action\&quot;: 2\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${access_token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>${destination}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${ims_url}/v1/inventory/stock</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.ims_url</defaultValue>
      <description></description>
      <id>ce5f0431-2d10-4529-9d6c-dd4a595dea8f</id>
      <masked>false</masked>
      <name>ims_url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.access_token</defaultValue>
      <description></description>
      <id>ca46f55e-b021-4c5c-be94-31272f921cde</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.destination</defaultValue>
      <description></description>
      <id>b4bcbfa2-456c-4dad-ae21-52c0acc29a0b</id>
      <masked>false</masked>
      <name>destination</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
