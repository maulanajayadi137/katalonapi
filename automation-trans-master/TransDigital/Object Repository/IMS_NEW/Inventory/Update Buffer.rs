<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Buffer</name>
   <tag></tag>
   <elementGuidId>832a89c9-a68a-475b-b1cb-dd04f2cf25a1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;product_sku\&quot;: \&quot;PIM0000011976\&quot;,\n    \&quot;store_code\&quot;: \&quot;EAG\&quot;,\n    \&quot;buffer\&quot;: 10\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${access_token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>${destination}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${ims_url}/v1/inventory/buffer</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.ims_url</defaultValue>
      <description></description>
      <id>4f56a061-d066-40a3-8c34-38b0317f0118</id>
      <masked>false</masked>
      <name>ims_url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.access_token</defaultValue>
      <description></description>
      <id>812c47f0-3cb2-4963-8939-954b26586850</id>
      <masked>false</masked>
      <name>access_token</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.destination</defaultValue>
      <description></description>
      <id>bd651c3e-f738-4967-a321-44d0da6007c9</id>
      <masked>false</masked>
      <name>destination</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
