<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create Order Header Aigner</name>
   <tag></tag>
   <elementGuidId>6ff05fa9-4b1a-4e9b-a952-1e2294115a11</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n   \&quot;order_id\&quot;: \&quot;TRTLTFI001003-1\&quot;,\n   \&quot;reference_number\&quot;: \&quot;TRTLTFI001002\&quot;,\n   \&quot;order_type\&quot;: 1,\n   \&quot;customer_email\&quot;: \&quot;customer@mail.com\&quot;,\n   \&quot;customer_name\&quot;: \&quot;customer\&quot;,\n   \&quot;customer_phone\&quot;: \&quot;080989898989\&quot;,\n   \&quot;shipping_address\&quot;: \&quot;Jl. Jakarta pusat no.1\&quot;,\n   \&quot;billing_address\&quot;: \&quot;Jl. Jakarta pusat no.1\&quot;,\n   \&quot;province\&quot;: \&quot;DKI Jakarta\&quot;,\n   \&quot;city\&quot;: \&quot;Jakarta Selatan\&quot;,\n   \&quot;district\&quot;: \&quot;Cilandak\&quot;,\n   \&quot;zip_code\&quot;: \&quot;12120\&quot;,\n   \&quot;source_channel\&quot;: 1,\n   \&quot;source_order\&quot;: 1,\n   \&quot;merchant_code\&quot;: \&quot;TRTLTFI001\&quot;,\n   \&quot;fulfillment_store\&quot;: \&quot;10011\&quot;,\n   \&quot;order_items\&quot;: [\n       {\n           \&quot;sku\&quot;: \&quot;12378945612344\&quot;,\n           \&quot;quantity\&quot;: 25,\n           \&quot;item_price\&quot;: 2000,\n           \&quot;item_disc_price\&quot;: 0,\n           \&quot;item_paid_price\&quot;: 2000,\n           \&quot;item_weight\&quot;: 25000,\n           \&quot;weight\&quot;: 1000\n       }\n   ]\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>omega</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://${url}/v1/orders</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>15559a5e-0c13-4979-ae32-d6c5dd281433</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
