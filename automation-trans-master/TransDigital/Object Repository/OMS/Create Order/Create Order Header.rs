<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create Order Header</name>
   <tag></tag>
   <elementGuidId>662d2efd-bc54-47d1-95ad-173c6f99f520</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;order_id\&quot;: \&quot;TRTLMRT001T001-1\&quot;,\n    \&quot;merchant_code\&quot;: \&quot;TRTLMRT001\&quot;,\n    \&quot;reference_number\&quot;: \&quot;TRTLMRT001T001\&quot;,\n    \&quot;order_type\&quot;: 2,\n    \&quot;order_date\&quot;: \&quot;2019-12-18 17:32:01\&quot;,\n    \&quot;logistic_courier_type\&quot;: 1,\n    \&quot;customer_email\&quot;: \&quot;customer@mail.com\&quot;,\n    \&quot;customer_name\&quot;: \&quot;customer\&quot;,\n    \&quot;customer_phone\&quot;: \&quot;080989898989\&quot;,\n    \&quot;shipping_address\&quot;: \&quot;Jl. Jakarta pusat no.1\&quot;,\n    \&quot;billing_address\&quot;: \&quot;Jl. +++++++++++++++++++++++++++++++ pusat no.1\&quot;,\n    \&quot;latitude\&quot;:-6.24059,\n    \&quot;longitude\&quot;:106.829977,\n    \&quot;province\&quot;: \&quot;DKI Jakarta\&quot;,\n    \&quot;city\&quot;: \&quot;Kota Jakarta Selatan\&quot;,\n    \&quot;district\&quot;: \&quot;Cilandak\&quot;,\n    \&quot;zip_code\&quot;: \&quot;12120\&quot;,\n    \&quot;source_channel\&quot;: 1,\n    \&quot;source_order\&quot;: 1,\n    \&quot;fulfillment_store\&quot;: \&quot;10012\&quot;,\n    \&quot;time_slot\&quot;: \&quot;2019-12-05 14:38:01\&quot;,\n    \&quot;sub_total\&quot;: 2000,\n    \&quot;shipping_fee\&quot;: 5000,\n    \&quot;grand_total\&quot;: 2000,\n    \&quot;order_items\&quot;: [\n        {\n            \&quot;sku\&quot;: \&quot;12378945612345\&quot;,\n            \&quot;quantity\&quot;: 4,\n            \&quot;ori_price\&quot;: 2000,\n            \&quot;sell_price\&quot;: 2000,\n            \&quot;disc_price\&quot;: 0,\n            \&quot;sub_total\&quot;: 2000,\n            \&quot;paid_price\&quot;: 2000,\n            \&quot;weight\&quot;: 10,\n            \&quot;total_weight\&quot;: 100\n        }\n    ],\n    \&quot;payment\&quot;: {\n        \&quot;master_payment_id1\&quot;: 1,\n        \&quot;pay_ref_number1\&quot;: \&quot;ADkajsd213\&quot;,\n        \&quot;amount\&quot;: 7000,\n        \&quot;split_payment\&quot;: 0,\n        \&quot;currency\&quot;: 1\n    }\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>omega</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://oms-tm-api-stg.ctdigital.id/v1/orders</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>f210a64d-c05d-4928-bb8b-c3298d704973</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>'Random String&quot;\n'</defaultValue>
      <description></description>
      <id>b53bd963-2996-4f8a-a1c9-10de2920d514</id>
      <masked>false</masked>
      <name>variable_rand</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
