<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Create Order Return</name>
   <tag></tag>
   <elementGuidId>e4cd4558-e7ba-4622-9812-d4d1e849a335</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;order_id\&quot;: \&quot;AO0012-1\&quot;,\r\n    \&quot;order_items\&quot;: [\r\n        {\r\n            \&quot;sku\&quot;: \&quot;SKU01559\&quot;,\r\n            \&quot;quantity\&quot;: 1,\r\n            \&quot;item_price\&quot;: 2000,\r\n            \&quot;item_disc_price\&quot;: 0,\r\n            \&quot;item_paid_price\&quot;: 2000\r\n        }\r\n    ],\r\n    \&quot;comment\&quot;: \&quot;\&quot;,\r\n    \&quot;reason\&quot;: \&quot;\&quot;\r\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>http://${url}/v1/orderreturn</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>da0a5ddb-0bd6-487f-94d1-11419dff90d0</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
