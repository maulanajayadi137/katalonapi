<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Group</name>
   <tag></tag>
   <elementGuidId>b0a911d9-663c-4b86-a254-f426ad4e5446</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;id\&quot;: 1,\n\t\&quot;email\&quot;: \&quot;admin@ct.co.id\&quot;,\n\t\&quot;password\&quot;: \&quot;admin\&quot;,\n\t\&quot;password_confirm\&quot;: \&quot;admin\&quot;,\n\t\&quot;name\&quot;: \&quot;Admin2\&quot;,\n\t\&quot;phone\&quot;: \&quot;6285621321546\&quot;,\n\t\&quot;merchant_code\&quot;: \&quot;TRTLMRT001\&quot;,\n\t\&quot;role_id\&quot;: 2,\n\t\&quot;group_id\&quot;: \&quot;1,2\&quot;,\n\t\&quot;active\&quot;: 1\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PATCH</restRequestMethod>
   <restUrl>http://${url}/v1/usergroup</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>43bb2eac-a00a-40ac-9dc8-6c160486e870</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
