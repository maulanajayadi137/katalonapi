<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>update current user</name>
   <tag></tag>
   <elementGuidId>31c06216-0ace-4ea8-b0db-5ee48c455530</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;id\&quot;: 1,\n\t\&quot;email\&quot;: \&quot;ariyo5@ct.co.id\&quot;,\n\t\&quot;password\&quot;: \&quot;admin\&quot;,\n\t\&quot;password_confirm\&quot;: \&quot;admin\&quot;,\n\t\&quot;name\&quot;: \&quot;Ariyo\&quot;,\n\t\&quot;phone\&quot;: \&quot;622123456789\&quot;,\n\t\&quot;merchant_code\&quot;: \&quot;TRTLMRT001\&quot;,\n\t\&quot;role_id\&quot;: 7,\n\t\&quot;group_id\&quot;: \&quot;1,2\&quot;,\n\t\&quot;active\&quot;: 1\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>omega</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PATCH</restRequestMethod>
   <restUrl>http://${url}/me</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>1c73b325-37b4-4afd-b88d-81563907c9d8</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
