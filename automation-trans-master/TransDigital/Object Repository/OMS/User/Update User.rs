<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update User</name>
   <tag></tag>
   <elementGuidId>1ae76792-01fd-460e-bc4a-2f7eab79dfdf</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n\t\&quot;id\&quot;: 1,\n\t\&quot;email\&quot;: \&quot;admin@ct.co.id\&quot;,\n\t\&quot;password\&quot;: \&quot;admin\&quot;,\n\t\&quot;password_confirm\&quot;: \&quot;admin\&quot;,\n\t\&quot;name\&quot;: \&quot;Admin2\&quot;,\n\t\&quot;phone\&quot;: \&quot;6285621321546\&quot;,\n\t\&quot;merchant_code\&quot;: \&quot;TRTLMRT001\&quot;,\n\t\&quot;role_id\&quot;: 2,\n\t\&quot;group_id\&quot;: \&quot;1,2\&quot;,\n\t\&quot;active\&quot;: 1\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PATCH</restRequestMethod>
   <restUrl>http://${url}/v1/users</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>6e2abd7c-4bc9-4b57-abf3-542e9df7b18d</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
