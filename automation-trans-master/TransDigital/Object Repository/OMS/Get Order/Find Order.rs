<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Find Order</name>
   <tag></tag>
   <elementGuidId>3eb143a5-c2de-45db-bbd2-962493b9a22c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\n    \&quot;order_id\&quot;: \&quot;\&quot;,\n    \&quot;reference_number\&quot;: \&quot;\&quot;,\n    \&quot;source_location\&quot;: 0,\n    \&quot;order_date_from\&quot;: \&quot;\&quot;,\n    \&quot;order_date_to\&quot;: \&quot;\&quot;,\n    \&quot;sku\&quot;: \&quot;\&quot;,\n    \&quot;customer_email\&quot;: \&quot;\&quot;,\n    \&quot;customer_phone\&quot;: \&quot;\&quot;,\n    \&quot;coupon_code\&quot;: \&quot;\&quot;,\n    \&quot;source_channel\&quot;: [1],\n    \&quot;source_order\&quot;: [3],\n    \&quot;currency\&quot;: 0,\n    \&quot;status\&quot;: 4,\n    \&quot;sub_status\&quot;: 0,\n    \&quot;filter_by\&quot;: 0,\n    \&quot;limit\&quot;: 10,\n    \&quot;page\&quot;: 1\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dest</name>
      <type>Main</type>
      <value>omega</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://${url}/v1/orders</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>b9e80169-c8f9-4628-a004-23df88b2ee2c</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
