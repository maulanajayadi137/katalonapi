<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Get order allocation</name>
   <tag></tag>
   <elementGuidId>55f3a691-51bd-4f63-b4cc-79b93a554bac</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;text&quot;: &quot;{\r\n    \&quot;merchant_code\&quot;: \&quot;TRTLMRT001\&quot;,\r\n    \&quot;reference_number\&quot;: \&quot;A1114\&quot;,\r\n    \&quot;order_type\&quot;: 2,\r\n    \&quot;logistic_courier_type\&quot;: 1,\r\n    \&quot;customer_id\&quot;: 1,\r\n    \&quot;customer_email\&quot;: \&quot;customer@mail.com\&quot;,\r\n    \&quot;customer_name\&quot;: \&quot;customer\&quot;,\r\n    \&quot;customer_phone\&quot;: \&quot;080989898989\&quot;,\r\n    \&quot;shipping_address\&quot;: \&quot;Jl. Jakarta pusat no.1\&quot;,\r\n    \&quot;billing_address\&quot;: \&quot;Jl. Jakarta pusat no.1\&quot;,\r\n    \&quot;longitude\&quot;: \&quot;\&quot;,\r\n    \&quot;latitude\&quot;: \&quot;\&quot;,\r\n    \&quot;province\&quot;:1,\r\n    \&quot;city\&quot;:1,\r\n    \&quot;district\&quot;:1,\r\n    \&quot;sub_district\&quot;:1,\r\n    \&quot;order_items\&quot;: [\r\n        {\r\n            \&quot;sku\&quot;: \&quot;SKU01557\&quot;,\r\n            \&quot;quantity\&quot;: 1,\r\n            \&quot;item_price\&quot;: 2000,\r\n            \&quot;item_disc_price\&quot;: 0,\r\n            \&quot;item_paid_price\&quot;: 2000\r\n        },\r\n        {\r\n            \&quot;sku\&quot;: \&quot;SKU01567\&quot;,\r\n            \&quot;quantity\&quot;: 1,\r\n            \&quot;item_price\&quot;: 2300,\r\n            \&quot;item_disc_price\&quot;: 100,\r\n            \&quot;item_paid_price\&quot;: 2200\r\n        }\r\n    ]\r\n}&quot;,
  &quot;contentType&quot;: &quot;application/json&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;
}</httpBodyContent>
   <httpBodyType>text</httpBodyType>
   <httpHeaderProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>http://${url}/v1/orderallocation</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.url</defaultValue>
      <description></description>
      <id>60695b5e-ec18-4e80-a6f4-ded99d2ed092</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
