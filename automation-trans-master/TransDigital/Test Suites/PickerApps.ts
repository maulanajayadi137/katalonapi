<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>PickerApps</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>82c1fb57-651c-45f7-a889-0ce6bab4f76c</testSuiteGuid>
   <testCaseLink>
      <guid>ca8e8f91-7d69-4445-ad19-8e2251146e41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/A-User/1_LoginUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b199e178-1f9b-4356-adb3-4a0ce1a95979</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/A-User/1_LoginUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f05b9683-6fa8-497a-962b-cb5525fe19b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A-User/2_GetOrderHeaders</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09ffdab4-2657-4767-b7ff-d2233dbbc188</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A-User/3_GetOrderDetails</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b15b920-b046-42d0-8b22-3ae76f0930ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A-User/4_GetAllMytask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c85418e5-d5d8-49be-a3af-cc26b86b5ffb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A-User/5_CreateTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18fcc90e-bfae-4065-9c2f-7aca6c939745</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/A-User/6_AddOrderMyTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29514ee2-95fc-4b0c-9411-a86f4b44c596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/A-User/7_Check_Order_NotMine</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
