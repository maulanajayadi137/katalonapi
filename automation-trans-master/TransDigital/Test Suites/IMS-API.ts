<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>IMS-API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>bfc9ef64-88a1-461b-9ae1-48b518c3d3de</testSuiteGuid>
   <testCaseLink>
      <guid>49641807-8b2b-4e6f-ac62-139a20caee18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Auth/0_login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40017c17-bee0-42e3-8349-8b5297f65e9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Store/1_createStore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33399fe4-0d81-418a-81b6-ccfed13d121a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Store/2_createBulk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a67d463a-f7b5-4b05-a363-8ad9c3a2b6c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Store/4_updateStoreBulk</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>248df1bb-c922-41b4-81bd-24862c954958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Store/5_disableStore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a6a6b67-751a-4bb8-a680-c2cd4a886d6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Store/6_deleteStore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e14a9b67-4a8f-4c93-b8b5-76d809eae82a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-IMS/Store/7_listStore</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
