<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>OMS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d4c176c2-5771-4d76-9236-73124be12840</testSuiteGuid>
   <testCaseLink>
      <guid>ebe7d631-f297-4185-a7f9-1299569ffa1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-OMS/0_Auth/login-API</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36ca48fa-3794-4a75-8883-6072fa324243</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-OMS/1_Create Order/createOrderHeader</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>363db0fe-fd4e-42f5-8578-92368cfcd4bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WebUI/OMS/login</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
