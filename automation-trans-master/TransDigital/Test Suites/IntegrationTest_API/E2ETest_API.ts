<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>E2ETest_API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>98790543-117e-4e0b-b65d-9597ecd76683</testSuiteGuid>
   <testCaseLink>
      <guid>89ddcdd3-d8b2-4da5-9f8e-a5bf1cdd4f10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-OMS/0_Auth/login-API</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e37bd71-cdbd-4e4d-9649-8f16c7c28370</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-OMS/1_Create Order/1_CreateOrder_Scene1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16ff95e2-821f-4605-a877-7f9ed64fd5d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-OMS/2_Update_Payment/UpdatePay_Scene1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e26cb25f-9410-4fc8-a84d-13ed7ac3bf70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-PICKPACK/1_Login_Picker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cd64a2b-66af-4fd6-9433-2c773d7b5b49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-PICKPACK/3_Order_Headers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02062701-30aa-4714-8ce9-e8d690be1adb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/API-PICKPACK/4_CreateTask</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
