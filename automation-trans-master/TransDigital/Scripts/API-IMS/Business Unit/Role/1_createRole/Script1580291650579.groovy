import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty


RequestObject req = findTestObject('Object Repository/IMS_NEW/Auth/Login')

req.setRestUrl('http://34.68.240.28/v1/role')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer-mysql'))

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "bearer "+token))

req.setHttpHeaderProperties(HTTPHeader)

json = [
	[
		"role_name": "User Admin 2",
		"role_module": [
			[
				"module_id": 1,
				"module_name": "User Management",
				"selected": true,
				"module_privilege": [
					[
						"privilege_id": 2,
						"privilege_name": "CREATE",
						"selected": true
					],
					[
						"privilege_id": 3,
						"privilege_name": "UPDATE",
						"selected": true
					],
					[
						"privilege_id": 1,
						"privilege_name": "VIEW",
						"selected": true
					],
					[
						"privilege_id": 4,
						"privilege_name": "DELETE",
						"selected": true
					]
				]
			],
			[
				"module_id": 2,
				"module_name": "Role Management",
				"selected": false,
				"module_privilege": [
					[
						"privilege_id": 3,
						"privilege_name": "UPDATE",
						"selected": false
					],
					[
						"privilege_id": 1,
						"privilege_name": "VIEW",
						"selected": false
					],
					[
						"privilege_id": 2,
						"privilege_name": "CREATE",
						"selected": false
					]
				]
			],
			[
				"module_id": 3,
				"module_name": "Location Management",
				"selected": false,
				"module_privilege": [
					[
						"privilege_id": 4,
						"privilege_name": "DELETE",
						"selected": false
					],
					[
						"privilege_id": 2,
						"privilege_name": "CREATE",
						"selected": false
					],
					[
						"privilege_id": 6,
						"privilege_name": "UPLOAD",
						"selected": false
					],
					[
						"privilege_id": 3,
						"privilege_name": "UPDATE",
						"selected": false
					],
					[
						"privilege_id": 1,
						"privilege_name": "VIEW",
						"selected": false
					]
				]
			],
			[
				"module_id": 4,
				"module_name": "Inventory Management",
				"selected": false,
				"module_privilege": [
					[
						"privilege_id": 3,
						"privilege_name": "UPDATE",
						"selected": false
					],
					[
						"privilege_id": 6,
						"privilege_name": "UPLOAD",
						"selected": false
					],
					[
						"privilege_id": 1,
						"privilege_name": "VIEW",
						"selected": false
					],
					[
						"privilege_id": 4,
						"privilege_name": "DELETE",
						"selected": false
					],
					[
						"privilege_id": 2,
						"privilege_name": "CREATE",
						"selected": false
					],
					[
						"privilege_id": 5,
						"privilege_name": "DOWNLOAD",
						"selected": false
					]
				]
			],
			[
				"module_id": 5,
				"module_name": "Buffer Management",
				"selected": false,
				"module_privilege": [
					[
						"privilege_id": 2,
						"privilege_name": "CREATE",
						"selected": false
					],
					[
						"privilege_id": 5,
						"privilege_name": "DOWNLOAD",
						"selected": false
					],
					[
						"privilege_id": 3,
						"privilege_name": "UPDATE",
						"selected": false
					],
					[
						"privilege_id": 6,
						"privilege_name": "UPLOAD",
						"selected": false
					],
					[
						"privilege_id": 1,
						"privilege_name": "VIEW",
						"selected": false
					],
					[
						"privilege_id": 4,
						"privilege_name": "DELETE",
						"selected": false
					]
				]
			],
			[
				"module_id": 6,
				"module_name": "Product Management",
				"selected": false,
				"module_privilege": [
					[
						"privilege_id": 2,
						"privilege_name": "CREATE",
						"selected": false
					],
					[
						"privilege_id": 6,
						"privilege_name": "UPLOAD",
						"selected": false
					],
					[
						"privilege_id": 3,
						"privilege_name": "UPDATE",
						"selected": false
					],
					[
						"privilege_id": 1,
						"privilege_name": "VIEW",
						"selected": false
					],
					[
						"privilege_id": 4,
						"privilege_name": "DELETE",
						"selected": false
					]
				]
			]
		]
	]
]


body = new JsonBuilder(json).toPrettyString()
req.setHttpBody(body)
def respon=WS.sendRequest(req)
WS.verifyResponseStatusCode(respon, 206)
//message": "Store code already exist
println respon
resp = WS.sendRequest(req).getResponseText()
//WS.verifyElementPropertyValue(resp, 'data.store_name', "Store code already exist.")
println resp



