import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty


RequestObject req = findTestObject('Object Repository/IMS_NEW/Auth/Login')

req.setRestUrl('http://34.68.240.28/v1/role')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer-mysql'))

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "bearer "+token))

req.setHttpHeaderProperties(HTTPHeader)

json = [
	[
    "role_id": 3,
    "role_name": "Administrator",
    "role_status": 1,
    "role_module": [
        [
            "module_id": 2,
            "module_name": "User",
            "selected": true,
            "module_privilege": [
                [
                    "privilege_id": 4,
                    "privilege_name": "Delete",
                    "selected": false
                ],
                [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": false
                ],
                [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
                ],
                [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ]
            ]
        ],
        [
            "module_id": 2,
            "module_name": "Role",
            "selected": true,
            "module_privilege": [
                [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
                ],
                [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ],
                [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": true
                ]
            ]
        ],
        [
            "module_id": 3,
            "module_name": "Store",
            "selected": true,
            "module_privilege": [
                [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ],
                [
                    "privilege_id": 4,
                    "privilege_name": "Delete",
                    "selected": true
                ],
                [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": true
                ],
                [
                    "privilege_id": 6,
                    "privilege_name": "Upload",
                    "selected": true
                ],
                [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
               ]
            ]
        ],
        [
            "module_id": 4,
            "module_name": "Inventory",
            "selected": true,
            "module_privilege": [
                [
                    "privilege_id": 5,
                    "privilege_name": "Download",
                    "selected": true
                ],
                [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
                ],
                [
                    "privilege_id": 6,
                    "privilege_name": "Upload",
                    "selected": true
                ],
                [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ],
                [
                    "privilege_id": 4,
                    "privilege_name": "Delete",
                    "selected": true
                ],
                [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": true
                ]
            ]
        ],
        [
            "module_id": 5,
            "module_name": "Buffer",
            "selected": true,
            "module_privilege": [
                [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": true
                ],
                [
                    "privilege_id": 5,
                    "privilege_name": "Download",
                    "selected": true
                ],
                [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
                ],
                [
                    "privilege_id": 6,
                    "privilege_name": "Upload",
                    "selected": true
                ],
                [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ],
                [
                    "privilege_id": 4,
                    "privilege_name": "Delete",
                    "selected": true
                ]
            ]
        ],
        [
            "module_id": 6,
            "module_name": "Product",
            "selected": true,
            "module_privilege": [
                [
                    "privilege_id": 4,
                    "privilege_name": "Delete",
                    "selected": true
                ],
                [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": true
                ],
                 [
                    "privilege_id": 6,
                    "privilege_name": "Upload",
                    "selected": true
                ],
                 [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
                ],
                [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ]
            ]
        ],
        [
            "module_id": 7,
            "module_name": "Group",
            "selected": true,
            "module_privilege": [
                 [
                    "privilege_id": 1,
                    "privilege_name": "View",
                    "selected": true
                ],
                 [
                    "privilege_id": 4,
                    "privilege_name": "Delete",
                    "selected": true
                ],
                 [
                    "privilege_id": 2,
                    "privilege_name": "Create",
                    "selected": true
                ],
                [
                    "privilege_id": 3,
                    "privilege_name": "Update",
                    "selected": true
                ]
            ]
        ]
    ]
]
]


body = new JsonBuilder(json).toPrettyString()
req.setHttpBody(body)
def respon=WS.sendRequest(req)
WS.verifyResponseStatusCode(respon, 206)
//message": "Store code already exist
println respon
resp = WS.sendRequest(req).getResponseText()
//WS.verifyElementPropertyValue(resp, 'data.store_name', "Store code already exist.")
println resp



