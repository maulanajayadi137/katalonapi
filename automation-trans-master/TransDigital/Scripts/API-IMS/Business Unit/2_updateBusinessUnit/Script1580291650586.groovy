import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty



for (i = 0; i <3; i++) {
	RequestObject req = findTestObject('Object Repository/IMS_NEW/Auth/Login')

req.setRestUrl('http://34.68.240.28/v1/businessunit')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer-mysql'))

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "bearer "+token))

req.setHttpHeaderProperties(HTTPHeader)

if (i==0){
json = [
	[
	"business_unit_id": 3,
	"business_unit_name": "Hugo Hudo000",
	"business_unit_desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ipsum non magna gravida rutrum ut ut augue. In feugiat at tellus in lacinia.",
	"business_unit_status": 1
				]
]}
else if (i==1){
	json = [
		[
	"business_unit_id": 3,
	"business_unit_name": "Hugo Hudo000",
	"business_unit_desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ipsum non magna gravida rutrum ut ut augue. In feugiat at tellus in lacinia.",
	"business_unit_status": 1
				]
	]
}


else if (i==2){
	json = [
			[
	"business_unit_id": 3,
	"business_unit_name": "Hugo Hudo000",
	"business_unit_desc": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet ipsum non magna gravida rutrum ut ut augue. In feugiat at tellus in lacinia.",
	"business_unit_status": 1
				]
	]
}

body = new JsonBuilder(json).toPrettyString()
req.setHttpBody(body)
def respon=WS.sendRequest(req)
if (respon==400){
WS.verifyResponseStatusCode(respon, 400)
//message": "Store code already exist
println respon
resp = WS.sendRequest(req).getResponseText()
println resp
}
else {
		WS.verifyResponseStatusCode(respon, 200)
		//message": "Store code already exist
		println respon
		resp = WS.sendRequest(req).getResponseText()
		println resp
		}
}