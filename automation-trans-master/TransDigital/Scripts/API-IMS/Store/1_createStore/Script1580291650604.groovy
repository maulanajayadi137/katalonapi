import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty



for (i = 0; i <3; i++) {
	RequestObject req = findTestObject('Object Repository/IMS_NEW/Auth/Login')

//req.setRestUrl('http://ims.api.stg.ctdigital.id/ims/v1/store')
req.setRestUrl('http://ims.api.stg.ctdigital.id:8787/ims-new/v1/store')
ArrayList HTTPHeader = new ArrayList()
//HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer'))
HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer-mysql'))

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "bearer "+token))

req.setHttpHeaderProperties(HTTPHeader)

if (i==0){
json = [[
//	"location_code": "EAG-CWS",
//	"offline_code": "EAG-CWS",
//	"name": "Aigner Ciputra World Surabaya",
//	"address": "Ciputra World Surabaya GF#Unit 2 Jl. Mayjen Sungkono No. 89 Surabaya 60225",
//	"zipcode": "60225",
//	"province": "JAWA TIMUR",
//	"city": "Kota Surabaya",
//	"district": "Dukuh Pakis",
//	"latitude":  -6.242,
//	"longitude": 106.828,
//	"Phone": "021-29200177",
//	"email": "store.eag.cws@transfashionindonesia.com",
//	"business_unit_code": "TRTLTFI001"
//	"store_id":"13",
    "store_code": "EAG-GLX",
    "offline_code": "EAG-GLX",
    "store_name": "Aigner Galaxy Mall Surabaya",
    "store_address": "Jl. Dharmahusada Indah Timur No.35-37",
    "store_zipcode": "60115",
    "province": "JAWA TIMUR",
    "city": "Kabupaten Lamongan",
    "district": "Kalitengah",
    "latitude": -7.2748136,
    "longitude": 112.7819927,
    "Phone": "0220000000",
    "email": "na4@transfashionindonesia.com",
    "business_unit_code": "TRTLTFI001"
]]}
else if (i==1){
	json = [[
//		"location_code": "EAG-CWS",
//		"offline_code": "EAG-CWS",
//		"name": "Aigner Ciputra World Surabaya",
//		"address": "Ciputra World Surabaya GF#Unit 2 Jl. Mayjen Sungkono No. 89 Surabaya 60225",
//		"zipcode": "60225",
//		"province": "JAWA TIMUR",
//		"city": "Kota Surabaya",
//		"district": "Dukuh Pakis",
//		"latitude":  -6.242,
//		"longitude": 106.828,
//		"Phone": "021-29200177",
//		"email": "store.eag.cws@transfashionindonesia.com",
//		"business_unit_code": "TRTLTFI001"
//		"store_id":"14",
		"store_code": "EAG-GC",
		"offline_code": "EAG-GC",
		"store_name": "Aigner Gandaria City",
		"store_address": "Aigner Gandaria City",
		"store_zipcode": "12240",
		"province": "DKI JAKARTA",
		"city": "Kota Jakarta Selatan",
		"district": "Kebayoran Lama",
		"latitude": -6.242,
		"longitude": 106.828,
		"Phone": "02129200177",
		"email": "na77@transfashionindonesia.com",
		"business_unit_code": "TRTLTFI001"
	]]
}


else if (i==2){  // Data Exixting in DB
	json = [[
//		"store_id":"15",
		"store_code": "EAG-BSM",
		"offline_code": "EAG-BSM",
		"store_name": "Aigner Bandung Super Mall",
		"store_address": "Bandung Super Mall GF#A049 Jl. Gatot Subroto, 289 Bandung 40273",
		"store_zipcode": "40273",
		"province": "JAWA BARAT",
		"city": "Kota Bandung",
		"district": "Batununggal",
		"latitude": -6.946683999999999,
		"longitude": 107.668137,
		"Phone": "02286012315",
		"email": "store.eag.bsm@transfashionindonesia.com",
		"business_unit_code": "TRTLTFI001"
//		"location_code": "EAG-CWS",
//		"offline_code": "EAG-CWS",
//		"name": "Aigner Ciputra World Surabaya",
//		"address": "Ciputra World Surabaya GF#Unit 2 Jl. Mayjen Sungkono No. 89 Surabaya 60225",
//		"zipcode": "60225",
//		"province": "JAWA TIMUR",
//		"city": "Kota Surabaya",
//		"district": "Dukuh Pakis",
//		"latitude":  -6.242,
//		"longitude": 106.828,
//		"Phone": "021-29200177",
//		"email": "store.eag.cws@transfashionindonesia.com",
//		"business_unit_code": "TRTLTFI001"
	]]
}

body = new JsonBuilder(json).toPrettyString()
req.setHttpBody(body)
//respon=WS.sendRequest(req)
//		println respon
		resp = WS.sendRequest(req).getResponseText()
		println resp
	
		}



//body = new JsonBuilder(json).toPrettyString()
//req.setHttpBody(body)
////respon=WS.sendRequest(req)
//////if (WS.verifyResponseStatusCode(respon, 200)) {
//////WS.verifyResponseStatusCode(respon, 200)
//////message": "Store code already exist
////
//////}
//////else {
//////		WS.verifyResponseStatusCode(respon, 206)
//////		//message": "Store code already exist
////		println respon
//		resp = WS.sendRequest(req).getResponseText()
//		println resp
//		}
