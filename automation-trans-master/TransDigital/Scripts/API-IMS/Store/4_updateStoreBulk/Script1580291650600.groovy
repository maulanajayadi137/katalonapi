import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder
import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.TestObjectProperty


RequestObject req = findTestObject('Object Repository/IMS_NEW/Auth/Login')

req.setRestUrl('http://34.68.240.28/v1/store/update/bulk')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer-mysql'))

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "bearer "+token))

req.setHttpHeaderProperties(HTTPHeader)

json = [
    "data": [
        [
            "store_id": 1,
		    "store_name": "halo",
		    "store_address": "Papua pinggir, deket galian freeport",
		    "store_zipcode": "12560",
		    "province": "PAPUA",
		    "city": "KOTA PAPUA",
		    "district": "PAPUA",
		    "latitude": -6.2904541,
		    "longitude": 106.796542,
		    "Phone": "021788401272",
		    "business_unit_code": "TRA",
		    "status": 1
        ]
    ]
]


body = new JsonBuilder(json).toPrettyString()
req.setHttpBody(body)
respon=WS.sendRequest(req)
WS.verifyResponseStatusCode(respon, 400)
//message": "Store code already exist
println respon
resp = WS.sendRequest(req).getResponseText()
//WS.verifyElementPropertyValue(resp, 'field.error.store_code', "Store code already exist.")
println resp



