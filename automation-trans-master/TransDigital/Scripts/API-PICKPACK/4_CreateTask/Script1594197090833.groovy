import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils
import org.apache.commons.io.FileUtils
import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import org.json.JSONObject
//for (i = 0; i <2; i++) {
RequestObject req = findTestObject('Object Repository/API/PickerApps/2_Order/1_get_orders_headers')

req.setRestUrl('https://pickpack-core-api-stg.ctdigital.id/picker/create/task/v1')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def jsonCreate = Utils.loadFile("BodyCreateTask.txt")
def tokenid = Utils.loadFile("token_id.txt")
req.setHttpHeaderProperties(HTTPHeader)
jsonBody = new JsonBuilder(jsonCreate).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
println resp 

def parsed = new JsonSlurper().parseText(resp)
//dataUID = parsed.toh_uid
//println dataUID
//Utils.saveFrom(dataUID, "GenData.txt")
//OrderHeader1 = parsed.items.order_header_uid
//println OrderHeader1
//Utils.saveFrom(OrderHeader1, "OrderHeader1.txt")
//OrderHeader2 = parsed.items.order_header_uid
//println OrderHeader2
//Utils.saveFrom(OrderHeader2, "OrderHeader2.txt")
//OrderHeader3 = parsed.items.order_header_uid
//println OrderHeader3
//Utils.saveFrom(OrderHeader3, "OrderHeader3.txt")
//OrderDetail1 = parsed.items.order_detail_uid
//println OrderDetail1
//Utils.saveFrom(OrderDetail1, "OrderDetail1.txt")
//OrderDetail2 = parsed.items.order_detail_uid
//println OrderDetail2
//Utils.saveFrom(OrderDetail2, "OrderDetail2.txt")
//OrderDetail3 = parsed.items.order_header_uid.order_detail_uid
//println OrderDetail3
//Utils.saveFrom(OrderDetail3, "OrderDetail3.txt")
dataUID = parsed.toh_uid
println dataUID
Utils.saveFrom(dataUID, "GenData.txt")


//bodyCompleteTask=[
//	
//		"token_id": "tokenid",
//		"toh_uid": "dataUID",
//		"task_status": 9,
//		"employee_id": "123456",
//		"employee_name": "Test",
//		"employee_position": "MJ",
//		"items": [
//			[
//				"order_header_uid": "880a5da6eb914c929c7bdb6d295fb7ac25950202003002016827mcbklihojf0238522613",
//				"order_detail_uid": "20200316200050273564",
//				"item_quantity": 2,
//				"item_status": 1
//			],
//			[
//				"order_header_uid": "880a5da6eb914c929c7bdb6d295fb7ac25950202003002016827mcbklihojf0238522613",
//				"order_detail_uid": "20200316200050273563",
//				"item_quantity": 2,
//				"item_status": 1
//			],
//			[
//				"order_header_uid": "4716a2c22ed441b09a6482790f65239834340202003002016446barpakwkcd9914816393",
//				"order_detail_uid": "20200316200040355206",
//				"item_quantity": 2,
//				"item_status": 1
//			]
//		]
//	
//	]
//
//
