import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

RequestObject req = findTestObject('Object Repository/API/PickerApps/1_User/1_Login_UserPickApps')
//req.setRestUrl('http/34.87.43.43/oms/logi:/n')
req.setRestUrl('https://pickpack-core-api-stg.ctdigital.id/sign/in/v1')
body = [('username') :'ooo-packer54@ctcd.com', ('password') :'Packer01@']
ArrayList HTTPHeader = new ArrayList()
//HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer'))
//HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'omega'))
////
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

req.setHttpHeaderProperties(HTTPHeader)

jsonBody = new JsonBuilder(body).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
println resp
def parsed = new JsonSlurper().parseText(resp)
if (parsed.code==200){
	println ("PASSED")
}

else {
	println ("FAILED")
}
token3 = parsed.token_id
println token3
Utils.saveFrom(token3, "token_id2.txt")

