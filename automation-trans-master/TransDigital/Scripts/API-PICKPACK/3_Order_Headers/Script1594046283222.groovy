import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils
import org.apache.commons.io.FileUtils
import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import org.json.JSONObject
//for (i = 0; i <2; i++) {
RequestObject req = findTestObject('Object Repository/API/PickerApps/2_Order/1_get_orders_headers')

req.setRestUrl('https://pickpack-core-api-stg.ctdigital.id/picker/get/order/headers/v1')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token2 = Utils.loadFile("GetOrder.txt")

req.setHttpHeaderProperties(HTTPHeader)
jsonBody = new JsonBuilder(token2).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
//String resp [] = string.split("\\r?\\n");
println resp 
//def slurper = new groovy.json.JsonSlurper()
//def result = slurper.parseText(resp)
//JsonSlurper slurper = new JsonSlurper()
//Map parsedJson = slurper.parseText(resp)
def parsed = new JsonSlurper().parseText(resp)
//data1 = parsed.user_id
//data2= parsed.order_action
def dataId = parsed.user_id
def orderAction=parsed.order_action

if (dataId.isEmpty() && orderAction.isEmpty()){
	def tokenid = Utils.loadFile("token_id.txt")
	
data = parsed.order_header_uid
println data



		bodyCreateTask=[
		"token_id":tokenid,
		"order_header_uids":data
		
		]
		Utils.saveFrom(bodyCreateTask,"BodyCreateTask.txt")
}
else {
	println ("Order sudah ada di menu Onprogress")
}
//if (parsed.code==200){
//	println ("PASSED")
//}
//
//else {
//	println ("FAILED")
//}

//String contents = new File ('BodyCreateTask.txt').getText('UTF-8')
//bodyCreateTask=[
//	"token_id":tokenid,
//	"order_header_uids":data
//	
//	]




