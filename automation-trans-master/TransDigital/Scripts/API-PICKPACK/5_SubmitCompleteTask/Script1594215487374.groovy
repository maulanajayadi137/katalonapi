import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils
import org.apache.commons.io.FileUtils
import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import org.json.JSONObject
//for (i = 0; i <2; i++) {
RequestObject req = findTestObject('Object Repository/API/PickerApps/2_Order/1_get_orders_headers')

req.setRestUrl('https://pickpack-core-api-stg.ctdigital.id/picker/create/task/v1')

ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def toh_uid = Utils.loadFile("GenData.txt")
def key = Utils.loadFile("token_id.txt")
def task_status=Utils.loadFile("OrderHeader1.txt")
def task_status=Utils.loadFile("OrderHeader2.txt")
def task_status=Utils.loadFile("OrderHeader3.txt")
def task_status=Utils.loadFile("OrderDetail1")
def task_status=Utils.loadFile("OrderDetail2")
def task_status=Utils.loadFile("OrderDetail3")

req.setHttpHeaderProperties(HTTPHeader)

body= [
		"token_id": key,
		"toh_uid": GenData,
		"task_status": Status,
		"employee_id": "123456",
		"employee_name": "MAUL",
		"employee_position": "GM",
		"items": [
			[
				"order_header_uid":OrderHeader1,
				"order_detail_uid": OrderDetail1,
				"item_quantity": 2,
				"item_status": 1
			],
			[
				"order_header_uid": OrderHeader2,
				"order_detail_uid": OrderDetail2,
				"item_quantity": 2,
				"item_status": 1
			],
			[
				"order_header_uid": OrderHeader3,
				"order_detail_uid": OrderDetail3,
				"item_quantity": 2,
				"item_status": 1
			]
		]
	]
	

//println body
//Utils.saveFrom(body,"BodyCreateTask.txt")

jsonBody = new JsonBuilder(body).toPrettyString()
req.setHttpBody(jsonBody)
println(req.getHttpBody())
//String jsonFormattedString = jsonBody.replaceAll("\\", "");

resp = WS.sendRequest(req).getResponseText()
println resp

jsonBody = new JsonBuilder(Data4).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
println resp 
