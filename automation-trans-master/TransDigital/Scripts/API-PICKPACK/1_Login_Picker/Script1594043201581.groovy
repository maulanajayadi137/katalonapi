import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import java.time.*
import groovy.time.TimeCategory
//import java.text.var1


RequestObject req = findTestObject('Object Repository/API/PickerApps/1_User/1_Login_UserPickApps')
//req.setRestUrl('http/34.87.43.43/oms/logi:/n')
req.setRestUrl('https://pickpack-core-api-stg.ctdigital.id/sign/in/v1')
body = [('username') :'ooo-picker54@ctcd.com', ('password') :'Picker01@']
ArrayList HTTPHeader = new ArrayList()
//HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'stargazer'))
//HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'omega'))
////
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

req.setHttpHeaderProperties(HTTPHeader)

jsonBody = new JsonBuilder(body).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
println resp
def parsed = new JsonSlurper().parseText(resp)
//if (parsed.code==200){
//	println ("PASSED")
//}
//
//else {
//	println ("FAILED")
//}
key = parsed.token_id
println key
Utils.saveFrom(key, "token_id.txt")
//
//println json.reference_number
//String text = json2.token_id;
//text = text.replaceAll("[^a-zA-Z0-9]", "");
def var1 = new Date()
json2=[
	"token_id":key,
	"last_modified_date":var1.format("yyyy-MM-dd ", TimeZone.getTimeZone('UTC'))+ "01:01:01.123456"
	//format("yyyy-MM-dd HH:mm:ss.SSSSSS", TimeZone.getTimeZone('UTC'))
	//format("MM/dd/yyyy'T'HH:mm:ss.SSSS'Z'")
	//format("yyyy-MM-dd ", TimeZone.getTimeZone('UTC'))+ "01:01:01.123456"
	]

println json2
Utils.saveFrom(json2,"GetOrder.txt")


body=[
	"token_id":key,
	
	]

println body
Utils.saveFrom(body,"BodyCreateTask.txt")
