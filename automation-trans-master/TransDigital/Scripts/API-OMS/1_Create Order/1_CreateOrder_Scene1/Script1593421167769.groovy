import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*

import org.junit.After
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils
import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import org.apache.commons.lang.RandomStringUtils
import java.text.SimpleDateFormat
import java.io.File
import com.kms.katalon.core.annotation.Keyword
//for (i = 0; i <2; i++) {

RequestObject req = findTestObject('Object Repository/OMS/Auth/login')


req.setRestUrl('https://oms-tm-api-stg.ctdigital.id/v1/orders')
ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'omega'))
//
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token1 = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "Bearer "+token1))

req.setHttpHeaderProperties(HTTPHeader)


Random rnd = new Random()
//println rnd
val1 = rnd.nextInt(1000);
println val1
//json="""[{"order_id":"TRTLMRT001T${val1-1}-1","merchant_code":"TRTLMRT001","reference_number":"TRTLMRT001T${val1}","order_type":2,"promotion_type":1,"promotion_value":100,"receiver_name":"Bambang","receiver_phone":"0809898989","order_date":"2020-07-01 14:56:01","logistic_courier_type":2,"customer_email":"Bambang@gmail.com","customer_name":"muhammad nur trianto Qahfi saDam","customer_phone":"080989898989","shipping_address":"Jl. Kapten Tendean No.12-14A, RT.2/RW.2, Jakarta SElatan","billing_address":"Jl. Kapten Tendean No.12-14A, RT.2/RW.2, Jakarta","latitude":-6.24059,"longitude":106.829977,"province":"DKI Jakarta","city":"Kota Jakarta Selatan","district":"Cilandak","zip_code":"12120","source_channel":1,"source_order":1,"fulfillment_store":"10059","time_slot":"2020-07-1 18:01:01","sub_total":20000,"shipping_fee":0,"grand_total":28000,"flag_spo":1,"coupon_code":"","order_items":[{"sku":"B12345678GM","quantity":2,"ori_price":500000,"sell_price":500000,"disc_price":0,"sub_total":500000,"paid_price":500000,"coupon_code":"","coupon_val":"","weight":10,"total_weight":100,"is_warehouse":false}],"payment":{"master_payment_id1":1010000021,"pay_ref_number1":"111","amount":255000,"split_payment":0,"currency":1}}]"""
json= [[
    "order_id": "TRTLMRT001T${val1-1}-1",
    "merchant_code": "TRTLMRT001",
    "reference_number": "TRTLMRT001T${val1}",
    "order_type": 2,
    "promotion_type": 1,
    "promotion_value": 100,
    "receiver_name": "Bambang",
    "receiver_phone": "0809898989",
    "order_date": "2020-07-01 14:56:01",
    "logistic_courier_type": 2,
    "customer_email": "Bambang@gmail.com",
    "customer_name": "muhammad nur trianto Qahfi saDam",
    "customer_phone": "080989898989",
    "shipping_address": "Jl. Kapten Tendean No.12-14A, RT.2/RW.2, Jakarta SElatan",
    "billing_address": "Jl. Kapten Tendean No.12-14A, RT.2/RW.2, Jakarta",
    "latitude": -6.24059,
    "longitude": 106.829977,
    "province": "DKI Jakarta",
    "city": "Kota Jakarta Selatan",
    "district": "Cilandak",
    "zip_code": "12120",
    "source_channel": 1,
    "source_order": 1,
    "fulfillment_store": "10059",
    "time_slot": "2020-07-1 18:01:01",
    "sub_total": 20000,
    "shipping_fee": 0,
    "grand_total": 28000,
    "flag_spo": 1,
    "coupon_code": "",
    "order_items": [
      [
        "sku": "B12345678GM",
        "quantity": 2,
        "ori_price": 500000,
        "sell_price": 500000,
        "disc_price": 0,
        "sub_total": 500000,
        "paid_price": 500000,
        "coupon_code": "",
        "coupon_val": "",
        "weight": 10,
        "total_weight": 100,
        "is_warehouse": false
      ]
    ],
    "payment": [
      "master_payment_id1": 1010000021,
      "pay_ref_number1": "111",
      "amount": 255000,
      "split_payment": 0,
      "currency": 1
        ]]]

jsonBody = new JsonBuilder(json).toPrettyString()
req.setHttpBody(jsonBody)
println(req.getHttpBody())
//String jsonFormattedString = jsonBody.replaceAll("\\", "");

resp = WS.sendRequest(req).getResponseText()
println resp



def parsed = new JsonSlurper().parseText(resp)
if (parsed.code==200){
	println ("PASSED")
}
else if(parsed.code==507){
	println ("DUPLICATE NUMBER")
	}
else {
	println ("FAILED")
}

println json.reference_number
String text = json.reference_number;
text = text.replaceAll("[^a-zA-Z0-9]", "");
Utils.saveFrom(text,"ref_number.txt")

println json.order_id
String text2 = json.order_id;
text = text.replaceAll("[^a-zA-Z0-9]", "");
Utils.saveFrom(text2, "merchantOrderID.txt")

json2=[
	"reference_number":text,
	"status": 2,
	"amount_adjustment": 0
]
Utils.saveFrom(json2,"UpdatePay.txt")

//
//println json.fulfillment_store
//String data = json.fulfillment_store;
//data = data.replaceAll("[^a-zA-Z0-9]", "");
//println data 
//
//println json.order_items.sku
//String data2 = json.order_items.sku;
//data2 = data2.replaceAll("[^a-zA-Z0-9]", "");
//println data2
//
//json3=[
//	"product_sku":data2,
//	"store_code":data,
//	"quantity":50,
//	"action":0
//	]
//Utils.saveFrom(json3,"/Users/maulanajayadi/Documents/automation-trans-master/TransDigital/UpdateStock.txt")
