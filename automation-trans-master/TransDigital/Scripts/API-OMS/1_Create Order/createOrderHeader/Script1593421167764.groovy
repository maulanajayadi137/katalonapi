import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper



for (i = 0; i <2; i++) {
RequestObject req = findTestObject('Object Repository/OMS/Auth/login')


req.setRestUrl('http://34.87.43.43/oms/v1/orders')
ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'omega'))
//
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token1 = Utils.loadFile("token1.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "Bearer "+token1))

req.setHttpHeaderProperties(HTTPHeader)

if (i==0){
json = [
    [
        "order_id": "TRTLMRT001T171-1",
        "merchant_code": "TRTLMRT001",
        "reference_number": "TRTLMRT001T171",
        "order_type": 2,
        "order_date": "2020-01-24 11:01:01",
        "logistic_courier_type": 1,
        "customer_email": "customer@mail.com",
        "customer_name": "customer",
        "customer_phone": "080989898989",
        "shipping_address": "Jl. Kapten Tendean No.12-14A, RT.2/RW.2",
        "billing_address": "Jl. Kapten Tendean No.12-14A, RT.2/RW.2",
        "latitude": -6.24059,
        "longitude": 106.829977,
        "province": "DKI Jakarta",
        "city": "Kota Jakarta Selatan",
        "district": "Cilandak",
        "zip_code": "12120",
        "source_channel": 1,
        "source_order": 1,
        "fulfillment_store": "10012",
        "time_slot": "2019-01-24 09:20:01",
        "sub_total": 250000,
        "shipping_fee": 5000,
        "grand_total": 255000,
        "order_items": [
            [
                "sku": "PIM0000011866",
                "quantity": 2,
                "ori_price": 500000,
                "sell_price": 500000,
                "disc_price": 0,
                "sub_total": 500000,
                "paid_price": 500000,
                "weight": 10,
                "total_weight": 100
            ]
        ],
        "payment": [
            "master_payment_id1": 1,
            "pay_ref_number1": "ADkajsd213",
            "amount": 255000,
            "split_payment": 0,
            "currency": 1
        ]]]
}
else if (i==1){
json =   [ [
        "order_id": "TRTLMRT001T172-1",
        "merchant_code": "TRTLMRT001",
        "reference_number": "TRTLMRT001T172",
        "order_type": 2,
        "order_date": "2020-01-24 11:01:19",
        "logistic_courier_type": 1,
        "customer_email": "customer@mail.com",
        "customer_name": "customer",
        "customer_phone": "080989898989",
        "shipping_address": "Jl. Kapten Tendean No.12-14A, RT.2/RW.2",
        "billing_address": "Jl. Kapten Tendean No.12-14A, RT.2/RW.2",
        "latitude": -6.24059,
        "longitude": 106.829977,
        "province": "DKI Jakarta",
        "city": "Kota Jakarta Selatan",
        "district": "Cilandak",
        "zip_code": "12120",
        "source_channel": 1,
        "source_order": 1,
        "fulfillment_store": "10012",
        "time_slot": "2019-12-05 14:38:01",
        "sub_total": 250000,
        "shipping_fee": 5000,
        "grand_total": 255000,
        "order_items": [
            [
                "sku": "PIM0000011832",
                "quantity": 2,
                "ori_price": 250000,
                "sell_price": 250000,
                "disc_price": 0,
                "sub_total": 250000,
                "paid_price": 255000,
                "weight": 10,
                "total_weight": 100
            ]
        ],
        "payment": [
            "master_payment_id1": 1,
            "pay_ref_number1": "ADkajsd213",
            "amount": 250000,
            "split_payment": 0,
            "currency": 1
        ]]]
	}

body = new JsonBuilder(json).toPrettyString()
req.setHttpBody(body)
//respon=WS.sendRequest(req)
//		println respon
		resp = WS.sendRequest(req).getResponseText()
		println resp
	
		//		}
		
		}
