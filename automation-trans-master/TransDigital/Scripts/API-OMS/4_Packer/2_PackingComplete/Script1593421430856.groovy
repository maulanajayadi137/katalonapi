import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

//for (i = 0; i <2; i++) {
//RequestObject req = findTestObject('Object Repository/OMS/Auth/login')
RequestObject req = findTestObject('Object Repository/OMS/Patch Order/Update Order Payment')


req.setRestUrl('https://oms-tm-api-stg.ctdigital.id/v1/payment')
ArrayList HTTPHeader = new ArrayList()

HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'omega'))
//
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def token1 = Utils.loadFile("token.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "Bearer "+token1))

req.setHttpHeaderProperties(HTTPHeader)

//if (i==0){
json = [
		"reference_number":"TRTLMRT001TES124",
		"status": 2,
		"action":3,
		"sub_action":7
		]
//}

jsonBody = new JsonBuilder(json).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
println resp
	

