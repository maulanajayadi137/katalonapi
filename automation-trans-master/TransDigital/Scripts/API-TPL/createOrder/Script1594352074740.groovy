import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils
import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import org.apache.commons.lang.RandomStringUtils
import java.text.SimpleDateFormat
import java.io.File
import com.kms.katalon.core.annotation.Keyword
//for (i = 0; i <2; i++) {

RequestObject req = findTestObject('Object Repository/TPL/order')


req.setRestUrl('https://tpl-api-stg.ctdigital.id/tpl/order')
ArrayList HTTPHeader = new ArrayList()
HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'tpl'))
//
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def authjwt = Utils.loadFile("JWT_TPL.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "Bearer "+authjwt))

req.setHttpHeaderProperties(HTTPHeader)

def ref = Utils.loadFile("ref_number.txt")
json= [   
	"service_type": 2,
    "order": [
        "id": "NextDays2",
        "reference_number": ref,
        "item": [
            [
                "weight": 2000,
                "price": 2000,
                "qty": 4
            ]
        ],
        "total_weight": 2000,
        "total_price": 2000,
        "total_qty": 4,
        "currency": ""
    ],
    "origin": [
        "name": "Transmart Lebak Bulus",
        "phone": "02178840127",
        "address": "Cibubur",
        "district": "Cempaka Putih",
        "city": "Kota Jakarta Pusat",
        "province": "DKI Jakarta",
        "latitude": -6.2415553,
        "longitude":  106.8276381
    ],
    "destination": [
        "name": "customer",
        "phone": "080989898989",
        "email": "customer@mail.com",
        "address": "RT.05/RW.04, Cibadak, Kec. Tanah Sereal, Kota Bogor, Jawa Barat 16166",
        "district": "Tanah Sereal",
        "city": "Kota Bogor",
        "province": "Jawa Barat",
        "postcode": "16166",
        "latitude": -6.2140923,
        "longitude": 106.8271495
    ]
]

jsonBody = new JsonBuilder(json).toPrettyString()
req.setHttpBody(jsonBody)
println(req.getHttpBody())
//String jsonFormattedString = jsonBody.replaceAll("\\", "");

resp = WS.sendRequest(req).getResponseText()
println resp



def parsed = new JsonSlurper().parseText(resp)
if (parsed.code==200){
	println ("PASSED")
}
else if(parsed.code==507){
	println ("DUPLICATE NUMBER")
}
else {
	println ("FAILED")
}

println json.data.awb
String text = json.data.awb;
text = text.replaceAll("[^a-zA-Z0-9]", "");

Utils.saveFrom(text,"awb.txt")

//
//println json.fulfillment_store
//String data = json.fulfillment_store;
//data = data.replaceAll("[^a-zA-Z0-9]", "");
//println data 
//
//println json.order_items.sku
//String data2 = json.order_items.sku;
//data2 = data2.replaceAll("[^a-zA-Z0-9]", "");
//println data2
//
//json3=[
//	"product_sku":data2,
//	"store_code":data,
//	"quantity":50,
//	"action":0
//	]
//Utils.saveFrom(json3,"/Users/maulanajayadi/Documents/automation-trans-master/TransDigital/UpdateStock.txt")
