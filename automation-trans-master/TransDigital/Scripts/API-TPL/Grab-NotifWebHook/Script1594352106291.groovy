import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils
import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper
import org.apache.commons.lang.RandomStringUtils
import java.text.SimpleDateFormat
import java.io.File
import com.kms.katalon.core.annotation.Keyword
//for (i = 0; i <2; i++) {

RequestObject req = findTestObject('Object Repository/TPL/order')


req.setRestUrl('https://tpl-api-stg.ctdigital.id/tpl/order')
ArrayList HTTPHeader = new ArrayList()
HTTPHeader.add(new TestObjectProperty('dest', ConditionType.EQUALS, 'tpl'))
//
HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

def authjwt = Utils.loadFile("JWT_TPL.txt")

HTTPHeader.add(new TestObjectProperty('Authorization', ConditionType.EQUALS, "Bearer "+authjwt))

req.setHttpHeaderProperties(HTTPHeader)
def awb = Utils.loadFile("awb.txt")
def order_id=Utils.liadFile("merchantOrderID.txt")

json= [
  "deliveryID": awb,
  "merchantOrderID": order_id,
  "timestamp": 1543305706,
  "status": "PICKING_UP",
  "trackURL": "https://express.grab.com/AkYkQfJ",
  "pickupPin": "4510",
  "failedReason": "",
  "sender": [
    "name": "Best Merchant",
    "address": "SuperMart Best, Jl. RS. Fatmawati No. 15 Komp. Golden Fatmawati Kel. Gandaria Selatan, Kec. Cilandak, Jakarta Selatan",
    "relationship": "Orang yang sebenarnya"
  ],
  "recipient": [
    "name": "Maulana Jayadi",
    "address": "Jl. Cipete Raya No.38, RT.6/RW.3, Cipete Sel., Cilandak, Kota Jakarta Selatan,Daerah Khusus Ibukota Jakarta 12410, Indonesia PT Rezeki surya intimakmur ",
    "relationship": "Orang yang sebenarnya"
  ],
  "driver": [
    "name": "ASAM Ahmadi Nurtrianto Ganteng",
    "phone": "6281994796387",
    "licensePlate": "B 3253 GM",
    "photoURL": "https://somephotourl.com/sgdfb6gfd87",
    "currentLat": -6.128631,
    "currentLng": 106.803085
  ]
]

jsonBody = new JsonBuilder(json).toPrettyString()
req.setHttpBody(jsonBody)
println(req.getHttpBody())
//String jsonFormattedString = jsonBody.replaceAll("\\", "");

resp = WS.sendRequest(req).getResponseText()
println resp



def parsed = new JsonSlurper().parseText(resp)
if (parsed.code==200){
	println ("PASSED")
}
else if(parsed.code==507){
	println ("DUPLICATE NUMBER")
}
else {
	println ("FAILED")
}

println json.reference_number
String text = json.reference_number;
text = text.replaceAll("[^a-zA-Z0-9]", "");


json2=[
	"reference_number":text,
	"status": 2,
	"amount_adjustment": 0
]
Utils.saveFrom(json2,"UpdatePay.txt")

//
//println json.fulfillment_store
//String data = json.fulfillment_store;
//data = data.replaceAll("[^a-zA-Z0-9]", "");
//println data 
//
//println json.order_items.sku
//String data2 = json.order_items.sku;
//data2 = data2.replaceAll("[^a-zA-Z0-9]", "");
//println data2
//
//json3=[
//	"product_sku":data2,
//	"store_code":data,
//	"quantity":50,
//	"action":0
//	]
//Utils.saveFrom(json3,"/Users/maulanajayadi/Documents/automation-trans-master/TransDigital/UpdateStock.txt")
