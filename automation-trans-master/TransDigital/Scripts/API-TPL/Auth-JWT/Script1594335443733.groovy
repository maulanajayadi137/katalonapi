import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static org.junit.Assert.*
import org.junit.Test as Test
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType as ConditionType
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.transdigital.Utils

import groovy.json.JsonBuilder as JsonBuilder
import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty
import internal.GlobalVariable as GlobalVariable
import groovy.json.JsonSlurper

RequestObject req = findTestObject('Object Repository/TPL/Oauth')
//req.setRestUrl('http://34.87.43.43/oms/login')
req.setRestUrl('https://tpl-api-stg.ctdigital.id/tpl/authorization/jwt')
body =[
	"grant_type": "client_credentials",
	"client_id": "4decd7840e4653b723c27fffee3abee1",
	"client_secret": "ee7aa6bae143446fe75b6d44f65975c6d738a94f1e8228da0c72299c3c1fde66",
	"refresh_token": "qhw7mQGMeYvkBU645RV6Pe6vri5h5Dtd"
]
ArrayList HTTPHeader = new ArrayList()


HTTPHeader.add(new TestObjectProperty('Content-Type', ConditionType.EQUALS, 'application/json'))

req.setHttpHeaderProperties(HTTPHeader)

jsonBody = new JsonBuilder(body).toPrettyString()

req.setHttpBody(jsonBody)

println(req.getHttpBody())

resp = WS.sendRequest(req).getResponseText()
println resp

def parsed = new JsonSlurper().parseText(resp)
//if (parsed.code==200){
//	println ("PASSED")
//}
//
//else {
//	println ("FAILED")
//}
token = parsed.data.access_token
println token
Utils.saveFrom(token, "JWT_TPL.txt")

