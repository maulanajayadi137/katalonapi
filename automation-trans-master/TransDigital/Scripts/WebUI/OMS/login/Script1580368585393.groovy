import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable



WebUI.switchToWindowUrl('http://35.198.206.155/auth')
'Click on \'Book Appointment\' button'
WebUI.click(findTestObject('Object Repository/OMS-WEB UI/login_btn'))

WebUI.click(findTestObject('Object Repository/OMS-WEB UI/toggle'))
WebUI.click(findTestObject('Object Repository/OMS-WEB UI/order_management'))
WebUI.click(findTestObject('Object Repository/OMS-WEB UI/search_order'))

WebUI.setText(findTestObject('Object Repository/OMS-WEB UI/input_orderid'),'TRTLMRT001T149')

WebUI.click(findTestObject('Object Repository/OMS-WEB UI/submit'))

WebUI.click(findTestObject('Object Repository/OMS-WEB UI/see_detail'))
//
//WebUI.verifyElementChecked(findTestObject('Object Repository/OMS-WEB UI/order_header'),'Order Detail for TRTLMRT001T149-1')
WebUI.verifyTextPresent("Order Detail for TRTLMRT001T149-1", true)

//WebUI.verifyTextPresent(findTestObject('Object Repository/OMS-WEB UI/verifyText_OrderId'),'TRTLMRT001T149-1', false)