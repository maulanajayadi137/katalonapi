package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p>Profile default : login_get_token</p>
     */
    public static Object core_dev
     
    /**
     * <p></p>
     */
    public static Object ims_url_dev
     
    /**
     * <p></p>
     */
    public static Object access_token
     
    /**
     * <p></p>
     */
    public static Object destination
     
    /**
     * <p></p>
     */
    public static Object ims_url_staging
     
    /**
     * <p></p>
     */
    public static Object ims_url_remote
     
    /**
     * <p></p>
     */
    public static Object url
     
    /**
     * <p></p>
     */
    public static Object statuspaypending
     
    /**
     * <p>Profile Development : This is the CoreBaseUrl for PickerApps</p>
     */
    public static Object CoreBaseURL
     
    /**
     * <p>Profile Development : user1</p>
     */
    public static Object token1
     
    /**
     * <p>Profile Development : user2</p>
     */
    public static Object token2
     
    /**
     * <p>Profile Development : Get Token</p>
     */
    public static Object paramLogin
     
    /**
     * <p></p>
     */
    public static Object ParamIDHeader
     
    /**
     * <p></p>
     */
    public static Object ParamOrderDetails
     
    /**
     * <p></p>
     */
    public static Object ParamCreateTask
     
    /**
     * <p></p>
     */
    public static Object ParamAddOrderTask
     
    /**
     * <p></p>
     */
    public static Object ParamAllMyTask
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            core_dev = selectedVariables['core_dev']
            ims_url_dev = selectedVariables['ims_url_dev']
            access_token = selectedVariables['access_token']
            destination = selectedVariables['destination']
            ims_url_staging = selectedVariables['ims_url_staging']
            ims_url_remote = selectedVariables['ims_url_remote']
            url = selectedVariables['url']
            statuspaypending = selectedVariables['statuspaypending']
            CoreBaseURL = selectedVariables['CoreBaseURL']
            token1 = selectedVariables['token1']
            token2 = selectedVariables['token2']
            paramLogin = selectedVariables['paramLogin']
            ParamIDHeader = selectedVariables['ParamIDHeader']
            ParamOrderDetails = selectedVariables['ParamOrderDetails']
            ParamCreateTask = selectedVariables['ParamCreateTask']
            ParamAddOrderTask = selectedVariables['ParamAddOrderTask']
            ParamAllMyTask = selectedVariables['ParamAllMyTask']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
